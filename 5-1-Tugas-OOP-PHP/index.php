<?php
// Parent class
trait Hewan {
  public $name;
  public $darah = 50;
  public $jumlahkaki;
  public $keahlian;

  public function atraksi()
  {
    echo "{$this->name} sedang {$this->keahlian}";
  }

}

abstract class Fight {

  use Hewan;

  public $attackPower;
  public $deffencePower;
  
  public function serang($Hewan)
  {
    echo "{$this->name} sedang menyerang {$Hewan->name}";
    echo "<br>";
    $Hewan->diserang($this);
  } 
  
  public function diserang($Hewan)
  {
    echo "{$this->name} diserang {$Hewan->name}";

    $this->darah = $this->darah - $Hewan->attackPower / $this->deffencePower;
  }

  protected function getInfo()
  {
    echo "nama : {$this->name}";
    echo "<br>";
    echo "jumlah kaki : {$this->jumlahkaki}";
    echo "<br>";
    echo "keahlian : {$this->keahlian}";
    echo "<br>";
    echo "darah : {$this->darah}";
    echo "<br>";
    echo "attack : {$this->attackPower}";
    echo "<br>";
    echo "deffen : {$this->deffencePower}";
  }


  abstract public function getInfoHewan();

}

// Child classes
class elang extends Fight {
  
  public function __construct($name)
  {
    $this->name = $name;
    $this->jumlahkaki = 2;
    $this->keahlian = "terbang tinggi";
    $this->attackPower = 10;
    $this->deffencePower = 5;
  }  

  public function getInfoHewan()
  {
    echo "jenis hewan : elang";
    echo "<br>";
    
    $this->getInfo();

    }

}


class harimau extends Fight {

  public function __construct($name)
  {
    $this->name = $name;
    $this->jumlahkaki = 4;
    $this->keahlian = "lari cepat";
    $this->attackPower = 7;
    $this->deffencePower = 8;
  }

  public function getInfoHewan()
  {
    echo "jenis hewan : harimau";
    echo "<br>";

    $this->getInfo();


    }
  
}

class spasi {
  public static function tampilkan()
  {
    echo "<br>";
    echo "=======";
    echo "<br>";
  }
}

// Create objects from the child classes

$harimau = new harimau("harimau");
$harimau->getInfoHewan();
spasi::tampilkan();

$elang = new elang("elang");
$elang->getInfoHewan();
spasi::tampilkan();

$harimau->serang($elang);
spasi::tampilkan();

$elang->getInfoHewan();
spasi::tampilkan();

$elang->serang($harimau);
spasi::tampilkan();

$harimau->getInfoHewan();